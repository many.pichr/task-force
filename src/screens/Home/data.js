export default [
    {
        "longitude": 104.86739974468946,
        "latitude": 11.763527031062603
    },
    {
        "longitude": 104.83611077070236,
        "latitude": 11.633311336302885
    },
    {
        "red": true,
        "longitude": 104.7130885720253,
        "latitude": 11.613808619104852
    },
    {
        "longitude": 104.79415532201529,
        "latitude": 11.474463569463698
    },
    {
        "red": true,
        "longitude": 104.88944415003061,
        "latitude": 12.017512903441341
    },
    {
        "longitude": 105.07504396140575,
        "latitude": 11.934036684698617
    },
    {
        "red": true,
        "longitude": 105.18668856471777,
        "latitude": 11.709916207976365
    },
    {
        "longitude": 105.02597764134407,
        "latitude": 11.75726129240052
    },
    {
        "longitude": 105.07504396140575,
        "latitude": 11.62982874975738
    },
    {
        "red": true,
        "longitude": 105.0793106853962,
        "latitude": 12.194118114479464
    },
    {
        "longitude": 104.89726647734642,
        "latitude": 12.257361496353418
    },
    {
        "longitude": 104.62348870933056,
        "latitude": 12.043246167089283
    },
    {
        "red": true,
        "longitude": 104.61993310600519,
        "latitude": 11.698775182111882
    },
    {
        "longitude": 104.59433309733868,
        "latitude": 11.34969124056201
    },
    {
        "red": true,
        "longitude": 104.8951331153512,
        "latitude": 11.322499248639218
    },
    {
        "longitude": 105.15539959073067,
        "latitude": 11.373395211020727
    },
    {
        "longitude": 105.1546884700656,
        "latitude": 11.245789114160035
    }
]
