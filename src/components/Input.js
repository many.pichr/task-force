import React, {Component, useEffect,useState,useRef} from 'react';
import {Platform, Picker, View, Text, TextInput, TouchableOpacity, Dimensions, BackHandler} from 'react-native';
import {Colors, Fonts} from '../utils/config';
import moment from 'moment';
import {Input} from 'react-native-elements'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DateTimePicker from '@react-native-community/datetimepicker';
import ReactNativePickerModule from 'react-native-picker-module';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ActionSheet from 'react-native-actions-sheet';
import Lang from '../Language';
const App = (props) => {
    const {values} = props.value;
    const [value,setValue] = useState(values[props.name]);
    const onChange = (val) => {
        setValue(val);
        props.handleInput(props.name,val)

    };
        return (<View style={{width:'100%'}}>
                        <Text style={{fontSize:RFPercentage(2.2),color:Colors.textColor,marginTop:5,fontFamily:Fonts.primary}}>{props.nolabel?"":props.label} {props.required&&<Text style={{color:'#ff514d'}}>*</Text>}</Text>
                        <TextInput
                        placeholder={props.title}
                        placeholderTextColor={Colors.primaryBlur}
                        multiline={props.textarea}
                        value={value}
                        numberOfLines={props.col?props.col:4}
                        onFocus={()=>props.onFocus&&props.onFocus(true)}
                        onBlur={()=>props.onFocus&&props.onFocus(false)}
                        keyboardType={props.number?'numeric':'default'}
                        onChangeText={val=>onChange(val)}
                        // onChangeText={value=>props.handleInput(props.name,value)}
                        labelStyle={[{color:Colors.textColor},{fontWeight:'normal'}]}
                        style={[{height:props.textarea?props.col?props.col*20:100:40,borderBottomWidth:1,borderBottomColor:Colors.primary,width:'100%'}]}

                    />
                    </View>);
}
export default App;
